from bs4 import BeautifulSoup
from datetime import datetime
from win10toast import ToastNotifier

import re
import copy
import requests
import time

DEBUG = False
DOLAR_SOURCE = 'http://api-contenidos.lanacion.com.ar/json/v2/economia/cotizacion'
TARGET_CURRENCY = 'DBNA'
REFRESH_TIME = 60
notification = ToastNotifier()

# Test code
"""import json
import random
TEST_JSON = '''[{{"papel":"{0}","descripcion":"r","compra":{1},"venta":{2}}}]'''
REFRESH_TIME = .15
DEBUG = True"""

class Currency:
    def __init__(self, json):        
        self.coin = json['papel']
        self.description = json['descripcion']
        self.buy = json['compra']
        self.sell = json['venta']

    def __str__(self):
        return 'Moneda:{} Compra:{} Venta:{}'.format(self.coin, self.buy, self.sell)

def parse_currencies(json):    
    currencies = {}
    for c in json:
        currency_data = Currency(c)
        currencies[currency_data.coin] = currency_data
    return currencies


def print_currencies(currencies):
    print(generate_log_info(currencies, 2))


def currency_log_format(currency, spaces):
    value = str(currency)
    if currency.coin == TARGET_CURRENCY:
        value = '>>>> {} <<<<'.format(value)
    else:
        value = ' '*spaces + value
    return value

def generate_log_info(currencies, spaces=5):
    currency_logs = [currency_log_format(x, spaces) for x in currencies.values()]
    header = '{}{}{}\n'.format('*'*10, datetime.now(), '*'*10)
    values = '\n'.join(currency_logs) 
    bottom = '\n'

    return header + values + bottom

class FakeJson:
    def __init__(self, value):
        self.value = value
    def json(self):
        return json.loads(self.value)

def get_currency():
    try:
        if DEBUG:
            return FakeJson(TEST_JSON.format(TARGET_CURRENCY, random.uniform(45, 47), random.uniform(45, 47)))
        return requests.get(DOLAR_SOURCE)
    except requests.exceptions.RequestException as e:
        print('Connection error ocurred:\n{}\n'.format(str(e)))
        return e 

def show_notification(old_value, new_value):
    change_direction = {True:'Suba', False:'Baja'}
    title = "{0} Dolar".format(change_direction[old_value < new_value])
    notification.show_toast(title, "${0:.2f}  era  ${1:.2f}".format(new_value, old_value))


def update_state(values):
    state = {}
    if values is None:
        print('No currency available to register states.')
        return    
    for k,v in values.items():
        state[k] = copy.deepcopy(v)
    return state

def main():    
    start_time = time.time()    
    log_file = open('currency_log.txt', 'a')

    print('*** Checks coin values from LaNacion API once per minute. (Logs can be found in "currency_log.txt") ***')
    
    currency_state = None

    while True:        
        page = get_currency()
        if isinstance(page, Exception):    
            log_file.write('Failure retrieving currency info:\n{}\n'.format(page))
        else:
            currencies = parse_currencies(page.json())
            target_currency = currencies[TARGET_CURRENCY]

            if currency_state is None:
                currency_state = update_state(currencies)
                
            elif target_currency.sell != currency_state[TARGET_CURRENCY].sell:
                show_notification(currency_state[TARGET_CURRENCY].sell, target_currency.sell)
            currency_state = update_state(currencies)
                
            print_currencies(currencies)            
            
            log_file.write(generate_log_info(currencies))
        log_file.flush()
        time.sleep(REFRESH_TIME - ((time.time() - start_time) % REFRESH_TIME))
    log_file.close()


if __name__ == '__main__':
    main()
